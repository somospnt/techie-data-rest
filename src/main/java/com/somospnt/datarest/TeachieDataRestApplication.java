package com.somospnt.datarest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeachieDataRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(TeachieDataRestApplication.class, args);
	}
}
