CREATE TABLE persona(
    id BIGINT IDENTITY NOT NULL PRIMARY KEY,
    nombre VARCHAR(20),
    apellido VARCHAR(20)
);


insert into persona (nombre, apellido)
values('Clancy', 'Wiggum'),
('Hans', 'Moleman'),
('Lenny', 'Leonard');