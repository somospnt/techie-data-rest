package com.somospnt.datarest.repository;

import com.somospnt.datarest.domain.Persona;
import com.somospnt.datarest.projection.PersonaProjection;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.RequestMapping;

@RepositoryRestResource(collectionResourceRel = "personas", path = "personas", excerptProjection = PersonaProjection.class)
public interface PersonaRepository extends CrudRepository<Persona, Long> {

    @RequestMapping(path = "findByNombreContains")
    List<Persona> findByNombreContains(@Param("nombre") String nombre);

}
