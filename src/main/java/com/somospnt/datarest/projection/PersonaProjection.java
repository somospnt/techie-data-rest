package com.somospnt.datarest.projection;

import com.somospnt.datarest.domain.Persona;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "personaNombre", types = {Persona.class})
public interface PersonaProjection {

    @Value("#{target.nombre} #{target.apellido}")
    String getNombreCompleto();

}
